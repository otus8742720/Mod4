﻿using Microsoft.EntityFrameworkCore;
using Mod4.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Configuration;
using Microsoft.Extensions.Configuration;


namespace Mod4.Domain
{
    internal class DataContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public string DbPath { get; }

        public DataContext() : base()
        {
            //DbPath = "Server=(localdb)\\mssqllocaldb;Database=Mod41;Trusted_Connection=True;MultipleActiveResultSets=true;";
        }
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Mod41;Trusted_Connection=True;MultipleActiveResultSets=true;");
            //optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=SchoolDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
