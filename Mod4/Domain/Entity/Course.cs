﻿using Mod4.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod4.Domain.Entity
{
    internal class Course : BaseEntity
    {
        public string Title { get; set; }
        public ICollection<Module>? Modules { get; set; }
        
    }
}
