﻿using Microsoft.EntityFrameworkCore;
using Mod4.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace Mod4.Domain.Repos;

internal class EFRepository<T> : IRepository<T> where T : BaseEntity
{
    private readonly DataContext _dataContext;

    public EFRepository()
    {
        _dataContext = new();
    }
    public async Task AddAsync(T entity)
    {
        await _dataContext.Set<T>().AddAsync(entity);
        await _dataContext.SaveChangesAsync();
    }

    public async Task AddRangeAsync(ICollection<T> entities)
    {
        await _dataContext.Set<T>().AddRangeAsync(entities);
        await _dataContext.SaveChangesAsync();
    }
    public async Task AddRange(ICollection<T> entities)
    {
        await _dataContext.Set<T>().AddRangeAsync(entities);
        await _dataContext.SaveChangesAsync();
    }


    public async Task DeleteAsync(int id)
    {
        T entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        if (entity != null)
        {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }
    }

    public async Task<IEnumerable<T>> GetAllAsync()
    {
        return await _dataContext.Set<T>().ToListAsync();
    }

    public async Task<T?> GetAsync(int id)
    {
        return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task UpdateAsync(T entity)
    {
        await _dataContext.SaveChangesAsync();
    }

    public async Task<int> CountAsync(T entity)
    {
        return await _dataContext.Set<T>().CountAsync();
    }

}
