﻿using Mod4.Domain.Entity;
using Mod4.Domain.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Mod4.Seed
{
    internal static class SeedEntities
    {

        private static EFRepository<Course> _courseRepo = new EFRepository<Course>();
        private static EFRepository<Module> _moduleRepo = new EFRepository<Module>();
        private static EFRepository<Subject> _subjectRepo = new EFRepository<Subject>();

        public static async Task Seed()
        {
            if (!SeedIsChosen())
            {
                return;
            }

            //DeleteCurrentData();

            Course[] courses = new Course[]
            {
                new Course{Title="C# Developer. Basic", 
                    Modules = new Module[]
                    {
                        new Module {Number=1, Title="Знакомство с C#" },
                        new Module {Number=2, Title="ООП и анонимные типы с методами"},
                        new Module {Number=3, Title="Алгоритмы и стили кодирования"},
                    }
                },
                new Course{Title="C# Developer. Professional",
                    Modules = new[]
                    {
                        new Module{Number=1, Title="Архитектура проекта и Базы данных"},
                        new Module{Number=2, Title="Клиент-серверная архитектура и микросервисы"},
                    }
                },
                new Course{Title="ASP.NET Core"},
                new Course{Title="Базы данных"},
                new Course{Title="Golang Developer. Professional"}

                //new Course{Id=1, Title="C# Developer. Basic"},
                //new Course{Id=2, Title="C# Developer. Professional"},
                //new Course{Id=3, Title="ASP.NET Core"},
                //new Course{Id=4, Title="Базы данных"},
                //new Course{Id=5, Title="Golang Developer. Professional"}
            };
            _courseRepo.AddRange(courses);

            //Module[] modules = new Module[]
            //{
            //    new Module{Number=1, Title="Знакомство с C#", CourseId=1},
            //    new Module{Number=2, Title="ООП и анонимные типы с методами", CourseId=1},
            //    new Module{Number=3, Title="Алгоритмы и стили кодирования", CourseId=1},
            //    new Module{Number=1, Title="Архитектура проекта и Базы данных", CourseId=2},
            //    new Module{Number=2, Title="Клиент-серверная архитектура и микросервисы", CourseId=2},
            //    new Module{Number=1,Title="Введение в ASP.NET Core и WebApi", CourseId=3},
            //    new Module{Number=2, Title="Разворачивание приложения в облаке", CourseId =3}
            //};

            ////Module[] modules = new Module[]
            ////{
            ////    new Module{Id=1, Number=1, Title="Знакомство с C#", CourseId=1},
            ////    new Module{Id=2, Number=2, Title="ООП и анонимные типы с методами", CourseId=1},
            ////    new Module{Id=3,Number=3, Title="Алгоритмы и стили кодирования", CourseId=1},
            ////    new Module{Id=4,Number=1, Title="Архитектура проекта и Базы данных", CourseId=2},
            ////    new Module{Id=5,Number=2, Title="Клиент-серверная архитектура и микросервисы", CourseId=2},
            ////    new Module{Id=6,Number=1,Title="Введение в ASP.NET Core и WebApi", CourseId=3},
            ////    new Module{Id=7,Number=2, Title="Разворачивание приложения в облаке", CourseId =3}
            ////};
            //_moduleRepo.AddRange(modules);

            //Subject[] subjects = new Subject[]
            //{
            //    new Subject{Number=1, Title="Вводное занятие", ModuleId = 1},
            //    new Subject{Number=2, Title="Среда разработки VisualStudio: интерфейс, базовый функционал", ModuleId=1},
            //    new Subject{Number=11, Title="Классы как основа C# // ДЗ", ModuleId=2},
            //    new Subject{Number=12, Title="Консультация общая", ModuleId=2},
            //    new Subject{Number=1, Title="Знакомство, рассказ о формате Scrum, краткий обзор курса // ДЗ", ModuleId=4 },
            //    new Subject{Number=2, Title="Архитектура проекта", ModuleId=4},
            //    new Subject{Number=7, Title="REST и RESTful API // ДЗ", ModuleId=5 },
            //    new Subject{Number=8, Title="Интеграция приложений", ModuleId=5}
            //};
            //_subjectRepo.AddRange(subjects);


            //Subject[] subjects = new Subject[]
            //{
            //    new Subject{Id=1, Number=1, Title="Вводное занятие", ModuleId = 1},
            //    new Subject{Id=2, Number=2, Title="Среда разработки VisualStudio: интерфейс, базовый функционал", ModuleId=1},
            //    new Subject{Id=3, Number=11, Title="Классы как основа C# // ДЗ", ModuleId=2},
            //    new Subject{Id=4, Number=12, Title="Консультация общая", ModuleId=2},
            //    new Subject{Id=5, Number=1, Title="Знакомство, рассказ о формате Scrum, краткий обзор курса // ДЗ", ModuleId=4 },
            //    new Subject{Id=6, Number=2, Title="Архитектура проекта", ModuleId=4},
            //    new Subject{Id=7, Number=7, Title="REST и RESTful API // ДЗ", ModuleId=5 },
            //    new Subject{Id=8, Number=8, Title="Интеграция приложений", ModuleId=5}
            //};
            //await _subjectRepo.AddRangeAsync(subjects);
        }

        private static async Task DeleteCurrentData()
        {
            var courses = await _courseRepo.GetAllAsync();
            foreach (var course in courses)
            {
                await _courseRepo.DeleteAsync(course.Id);
            }
        }

        private static bool SeedIsChosen()
        {
            Console.Write("Do you want to fill DB with new data? (y/n) ");
            if (Console.ReadLine().ToLower() == "y" )
            {
                Console.WriteLine("New data will be created in DB");
                return true;
            }
            Console.WriteLine("Existing data will be kept in DB");
            return false;
        }

    }
}
