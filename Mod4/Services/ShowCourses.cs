﻿using Mod4.Domain.Entity;
using Mod4.Domain.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod4.Services
{
    internal static class ShowCourses
    {
        internal static void CoursesOnly()
        {
            Console.WriteLine("Список курсов:");
            EFRepository<Course> courseRepo = new EFRepository<Course>();
            var courses = courseRepo.GetAllAsync().Result;
            foreach (var course in courses)
            {
                Console.WriteLine($"Id: {course.Id}, Title: {course.Title}");
            }
            Console.WriteLine("- - -");
        }

        internal static void CoursesModulesSubjects()
        {
            Console.WriteLine("Список курсов с модулями и темами:");
            EFRepository<Course> courseRepo = new EFRepository<Course>();
            EFRepository<Module> moduleRepo = new EFRepository<Module>();
            EFRepository<Subject> subjectRepo = new EFRepository<Subject>();
            var courses = courseRepo.GetAllAsync().Result;
            foreach (var course in courses)
            {
                Console.WriteLine($"Id: {course.Id}, Title: {course.Title}");
                var modules = moduleRepo.GetAllAsync().Result.Where(m => m.CourseId ==course.Id);
                foreach (var module in modules)
                {
                    Console.WriteLine($"\tModule: {module.Number}. {module.Title}");
                    var subjects = subjectRepo.GetAllAsync().Result.Where(s => s.ModuleId ==module.Id);
                    foreach (var subject in subjects)
                    {
                        Console.WriteLine($"\t\tSubject: {subject.Number}. {subject.Title}");
                    }
                }
            }
            Console.WriteLine("- - -");
        }


    }
}
