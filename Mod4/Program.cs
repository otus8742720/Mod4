﻿using Mod4.Seed;
using Mod4.Services;

namespace Mod4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Здесь можно посмотреть список курсов, модулей и тем.\n");
            SeedEntities.Seed();
            TopLevelMenu.Show();
        }
    }
}