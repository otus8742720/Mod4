﻿using Microsoft.EntityFrameworkCore;
using Mod4_DirtyArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod4_DirtyArchitecture.Services;

internal static class ShowCourses
{
    internal static void CoursesOnly()
    {
        Console.WriteLine("Список курсов:");
        using DataContext ctx = new();
        var courses = ctx.Courses.OrderBy(c => c.Id).ToList();
        foreach (var course in courses)
        {
            Console.WriteLine($"Id: {course.Id}, Title: {course.Title}");
        }
        Console.WriteLine("- - -");
    }

    internal static void CoursesModulesSubjects()
    {
        Console.WriteLine("Список курсов с модулями и темами:");
        using DataContext ctx = new();
        var courses = ctx.Courses.OrderBy(c => c.Id).Include(x => x.Modules.OrderBy(m => m.Number))
            .ThenInclude(m => m.Subjects.OrderBy(s => s.Number)).ToList();
        foreach (var course in courses)
        {
            Console.WriteLine($"Id: {course.Id}, Title: {course.Title}");
            foreach (var module in course.Modules)
            {
                Console.WriteLine($"\tModule: {module.Number}. {module.Title}");
                foreach (var subject in module.Subjects)
                {
                    Console.WriteLine($"\t\tSubject: {subject.Number}. {subject.Title}");
                }
            }
        }
        Console.WriteLine("- - -");
    }

    internal static void CoursesModulesSubjects(int courseId)
    {
        Console.WriteLine("Курс с модулями и темами:");
        using DataContext ctx = new();
        var courses = ctx.Courses.Where(c => c.Id == courseId).Include(x => x.Modules.OrderBy(m => m.Number))
            .ThenInclude(m => m.Subjects.OrderBy(s => s.Number)).ToList();
        foreach (var course in courses)
        {
            Console.WriteLine($"Id: {course.Id}, Title: {course.Title}");
            foreach (var module in course.Modules)
            {
                Console.WriteLine($"\tModule: {module.Number}. {module.Title}");
                foreach (var subject in module.Subjects)
                {
                    Console.WriteLine($"\t\tSubject: {subject.Number}. {subject.Title}");
                }
            }
        }
        Console.WriteLine("- - -");
    }

}
