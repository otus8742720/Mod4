﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Mod4_DirtyArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Mod4_DirtyArchitecture.Services;

internal static class SeedEntities
{

    public static async Task Seed()
    {
        if (!SeedIsChosen())
        {
            return;
        }

        using DataContext ctx = new();
        ctx.Courses.RemoveRange(ctx.Courses);
        ctx.SaveChanges();

        Course[] courses = new Course[]
        {
            new Course{Title="C# Developer. Basic",
                Modules = new Module[]
                {
                    new Module {Number=1, Title="Знакомство с C#",
                        Subjects = new Subject[]
                        {
                            new Subject {Number=1, Title="Вводное занятие"},
                            new Subject {Title="Среда разработки VisualStudio: интерфейс, базовый функционал" }
                        }
                    },
                    new Module {Number=2, Title="ООП и анонимные типы с методами",
                        Subjects = new Subject[]
                        {
                            new Subject {Number=11, Title="Классы как основа C# // ДЗ"},
                            new Subject {Number=12, Title="Консультация общая"}
                        }
                    },


                    new Module {Number=3, Title="Алгоритмы и стили кодирования",
                        Subjects = new Subject[]
                        {
                            new Subject {Number=20, Title="Анализ сложности алгоритмов и сортировка"},
                        }
                    },
                }
            },
            new Course{Title="C# Developer. Professional",
                Modules = new[]
                {
                    new Module{Number=1, Title="Архитектура проекта и Базы данных",
                        Subjects = new Subject[]
                        {
                            new Subject {Number=1, Title="Знакомство, рассказ о формате Scrum, краткий обзор курса // ДЗ"},
                            new Subject {Number=2, Title="Архитектура проекта"},
                        }
                    },

                    new Module{Number=2, Title="Клиент-серверная архитектура и микросервисы",
                        Subjects = new Subject[]                     {
                            new Subject {Number=7, Title="REST и RESTful API // ДЗ"},
                            new Subject {Number=8, Title="Интеграция приложений"},
                        }
                    },
                }
            },
            new Course{Title="ASP.NET Core"},
            new Course{Title="Базы данных"},
            new Course{Title="Golang Developer. Professional"}

        };
        ctx.AddRange(courses);
        ctx.SaveChanges();
    }

    private static bool SeedIsChosen()
    {
        Console.Write("Do you want to fill DB with new data? (y/n) ");
        if (Console.ReadLine()?.ToLower() == "y" )
        {
            Console.WriteLine("New data will be created in DB");
            return true;
        }
        Console.WriteLine("Existing data will be kept in DB");
        return false;
    }
}
