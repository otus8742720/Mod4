﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod4_DirtyArchitecture.Services;

internal static class TopLevelMenu
{
    internal static void Show()
    {
        while (true)
        {
            Console.Write("\n1. Список курсов\n2. Список курсов с модулями и темами\n3. Добавление нового модуля\n" +
                "4. Добавление нового модуля\nЛюбой другой ввод - завершение работы\n\nВведите число: ");
            switch(Console.ReadLine())
            {
                case "1":
                    ShowCourses.CoursesOnly(); 
                    break;
                case "2":
                    
                    ShowCourses.CoursesModulesSubjects();
                    break;
                case "3":
                    ModuleServices.AddModule();
                    break;
                case "4":
                    ModuleServices.RemoveModule();
                    break;
                default:
                    return;
            }
        }
    }
}
