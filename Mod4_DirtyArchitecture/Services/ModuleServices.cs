﻿using Mod4_DirtyArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod4_DirtyArchitecture.Services
{
    internal class ModuleServices
    {
        internal static void AddModule()
        {
            using DataContext ctx = new();
            Console.WriteLine("Вот номера и названия курсов, в которые можно добавить модуль:");

            Course course = GetCourse(ctx);
            if (course == null) 
            {
                Console.WriteLine("В списке курсов нет курса с таким номером. Попробуйте в другой раз..");
                return;
            }

            course.Modules ??= new List<Module>();

            Module module = new()
            {
                CourseId = course.Id
            };
            if (int.TryParse(ReadString("Введите номер модуля: "), out int modNumber))
            {
                module.Number = modNumber;
            }
            module.Title = ReadString("Введите название модуля: ")?? "No Name";
            module.Description = ReadString("Введите описание модуля: ");
            module.Subjects = new List<Subject>();

            // Здесь, наверное, уместна была бы какая-то логика, запрещающая модулям иметь одинаковые номера или увеличивающая
            // номера последующих модулей на 1 в случае добавления модуля с номером, который уже есть в списке
            // Но пока модули могут иметь одинаковый номер..
            
            course.Modules.Add(module);
            ctx.SaveChanges();

            ShowCourses.CoursesModulesSubjects(course.Id);

        }

        private static Course GetCourse(DataContext ctx)
        {
            ShowCourses.CoursesOnly();
            Console.Write("Введите номер курса: ");

            if (!int.TryParse(Console.ReadLine(), out int userInput))
            {
                Console.WriteLine("Номер курса должен быть числом из списка, приведенного выше. Попробуйте в другой раз..");
                return null;
            }

            return ctx.Courses.FirstOrDefault(c => c.Id == userInput);
        }

        private static string ReadString(string v)
        {
            Console.Write(v);
            return Console.ReadLine();
        }

        internal static void RemoveModule()
        {
            using DataContext ctx = new();

            Console.WriteLine("Вот номера и названия курсов, из которых можно удалить модуль:");

            Course course = GetCourse(ctx);
            if (course == null)
            {
                Console.WriteLine("В списке курсов нет курса с таким номером. Попробуйте в другой раз..");
                return;
            }

            Console.WriteLine("Вот подробная информация о курсе:");
            ShowCourses.CoursesModulesSubjects(course.Id);
            Module module = GetModule(ctx, course.Id);
            if (module == null)
            {
                Console.WriteLine("В списке модулей курса нет модуля с таким номером. Попробуйте в другой раз..");
                return;
            }
            course.Modules.Remove(module);
            ctx.SaveChanges();

            Console.WriteLine("\nИнформация о курсе после удаления модуля");
            ShowCourses.CoursesModulesSubjects(course.Id);
        }

        private static Module GetModule(DataContext ctx, int courseId)
        {
            Console.Write("Введите номер модуля: ");

            if (!int.TryParse(Console.ReadLine(), out int userInput))
            {
                Console.WriteLine("Номер курса должен быть числом из списка, приведенного выше. Попробуйте в другой раз..");
                return null;
            }
            return ctx.Modules.First(m => m.CourseId == courseId && m.Number == userInput);

        }
    }
}
