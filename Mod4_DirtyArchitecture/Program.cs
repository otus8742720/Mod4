﻿using Mod4_DirtyArchitecture;
using Mod4_DirtyArchitecture.Services;

namespace Mod4_DirtyArchitecture;

internal class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Здесь можно посмотреть список курсов, модулей и тем.\n");

        SeedEntities.Seed();
        TopLevelMenu.Show();
    }
}