﻿namespace Mod4_DirtyArchitecture.Models;

internal class Module
{
    public int Id { get; set; }
    public int Number { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public int CourseId { get; set; }
    public ICollection<Subject>? Subjects { get; set; }
}

