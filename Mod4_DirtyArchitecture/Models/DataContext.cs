﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Mod4_DirtyArchitecture.Models
{
    internal class DataContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public DataContext() : base() {}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // for testing with MS SQL localdb 
           // optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Mod41Dirty;Trusted_Connection=True;MultipleActiveResultSets=true;");

           // for testing with PGSQL
            optionsBuilder.UseNpgsql("Server=192.168.1.47;Port=5432;Database=Mod4_Dirty;User Id=postgres;Password=postgres;");

            //uncomment this for debug only
            //optionsBuilder.LogTo(Console.WriteLine).EnableSensitiveDataLogging();
        }

    }
}
